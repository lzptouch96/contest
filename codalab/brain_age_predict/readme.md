# dataset
# method 
We use model fusion method for training and predicting. The basic model that we use is the GradientBoostingRegressor.

First, we divided the EO and EC data into three parts respectively. Second, we trained six separate model for each data. Then, we used the above model to predict the verification set. And the predicted results are fused to get the final result.The fusion method was weighted average.
